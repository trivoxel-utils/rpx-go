# RPX Deck

## About

A simple tool that helps you scan your Wii U games library and find the title IDs of your games.

With just a title ID (provided by the program), the program will launch the Wii U title in fullscreen automagically!

*Notice: This is still early access software and is not yet fully featured or stable. It should work fine enough though!*

## Current features

* Simple command line interface
* `rpxdeck list` shows see installed games. (add the `--verbose` flag for even more info!)
* Copy the title ID and paste it in `rpxdeck run <title_id_here>` to launch the games

## How to comprehensively add a Wii U game to Steam

1. The script can be run from anywhere but, the easiest way to use it is to copy it somewhere like `/usr/sbin/` or `/usr/bin`.
2. This can be done by right-clicking the file in your file manager (ie. Dolphin) or Files in GNOME and viewing the file properties!
You can use a command like `chmod +x ./path/to/rpxdeck` as well.
3. In desktop mode, open Steam. In the Library tab, click "Add a game", then "Add a non-Steam game..."
4. Choose the `rpxdeck` file. (you may need to show all files)
5. Search for "rpxdeck" in your Steam library
6. Right click on it and click "Properties"
7. Name it however you want.
8. Under "launch options", put `run <title_id>`.
If you don't know the title ID, run `rpxdeck list` in your game directory and copy/paste it.
9. Under "Start in" browse to your Wii U games folder.

## Optional step: Setting up cover art

_NOTE: This may become easier once I implement the automated importer. Star this project if you really want it._
_IDK why Valve made it so difficult!_

1. Set an icon in the properties.
2. Right click the background in the game on Steam to add a background and/or logo.
3. To set a vertical cover art:
    1. Search for your game.
    2. Click on the word "GAMES" in the search panel.
    3. Scroll to your game.
    4. Right click the game and choose a custom cover art.
4. To set a horizontal cover art:
    1. Start the game.
    2. CLick on the home tab in Steam.
    3. Your game should appear first under the "Recent games" section.
    4. Right click and choose custom artwork.

You need to play the game once to see the horizontal art to right click and set a custom artwork for that.

## List of planned features

* Easy install script
* Built-in setup wizard (to choose game dir)
* Automatic `.desktop` shortcut generator (so you can easily get your games )
* Automatic Steam Big Picture Mode importer using game assets
* Add multiple game source directories
* Bug fixes & enhancements


Check the `TODO` entries in the file to see what all features can be expected and to see what is currently working.
Some features may change in the future to expand functionality or for performance and stability reasons.
All TODOs are marked "low" through "high" depending on their priority.